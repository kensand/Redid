#!/bin/bash
rm -rf build
mkdir build
cython --embed -o build/Redid.c Redid.py
gcc build/Redid.c -I /usr/include/python3.7m/ -L /usr/lib/libpython3.7m.so -lpython3.7m -o build/Redid
