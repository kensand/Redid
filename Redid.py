#!/usr/bin/env python

import os
import io
import praw
#from PyQt5.QtWidgets import *
#from PyQt5.QtGui import *
#from PyQt5.QtWebKitWidgets import *
#from PyQt5.QtCore import *
from PyQt5 import QtCore, QtGui, QtWidgets
from PyQt5.QtCore import *
from PyQt5.QtGui import *
from PyQt5.QtWidgets import *
from PyQt5.QtWebKit import *
from PyQt5.QtWebKitWidgets import *
from PyQt5.QtWidgets import QApplication, QWidget, QMainWindow
from PyQt5.QtMultimedia import *
from PyQt5.QtMultimediaWidgets import *
from PIL import Image
from PIL.ImageQt import ImageQt
#import botCredentials
import requests
import pickle
import socket
import requests.auth

app_client_ID = 'LAZgGxeb4CN41Q'
app_secret = 'a99SkrJAKbxybZrPeW6NjmKL_pE'
fg = 'white'
bg = 'black'

content = (500, 500)

PORT = 38792
historyName = "history.pkl"
app = QApplication([])
layout = QtWidgets.QGridLayout()
n = QPushButton("Next")
print("Supported image formats:", QImageReader.supportedImageFormats ())
layout.addWidget(n)
poststream = None
post = None
im = None
tkpi = None
title = None
label = None
sublabel = None


class Browser(QWebView):

    def __init__(self):
        QWebView.__init__(self)
        self.loadFinished.connect(self._result_available)

    def _result_available(self, ok):
        frame = self.page().mainFrame()
        print(unicode(frame.toHtml()).encode('utf-8'))


def openPickleJar(filename):
    ret = None
    if os.path.isfile(filename):
        with open(filename, 'rb') as f:
            ret = pickle.load(f)
    return ret

def writePickleJar(filename, pickl):
    with open(filename, 'wb') as f:
        pickle.dump(pickl, f)



history = openPickleJar(historyName)
if history is None:
    history = list()

    
def exitProg(code=0):
    writePickleJar(historyName, history)
    exit(code)

def printnew():
    global results, label, frags, poststream, PORT
    if len(results):
        print(results)
    print("URL: ", label.url().toString())
    if label.url().toString().startswith('http://localhost:' + str(PORT)):
        localurl, fragstr = str.split(label.url().toString(), '?')
        fragments = str.split(fragstr, '&')
        print(label.url().fragment())
        print(fragments)
        for fragment in fragments:
            key, value = str.split(fragment, '=', 1)
            frags[key]=value

        timer.stop()
        print("FRAGMENT: ", frags)
        refresh_tok = reddit.auth.authorize(frags['code'])
        print("REFRESHTOK: ", refresh_tok)
        #reddit.auth.implicit(frags['access_token'], int(frags['expires_in']), frags['scope'])

        poststream = reddit.front.hot()
        nextFunc()
        print(reddit.user.me())
        writePickleJar('.Redidauth.pkl', refresh_tok)
        return

    if label.url().toString() == 'https://www.reddit.com/login':
        label.load(QUrl(url))


def getAuthorizationURL():


    global timer, label, socket, PORT, results
    #print(label.url())
    #timer.stop()





    HOST = 'localhost'                 # Symbolic name meaning all available interfaces
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    s.bind((HOST, PORT))
    s.listen(1)
    print('thread listening')
    conn, addr = s.accept()
    print('Connected by', addr)
    message = bytearray()

    
    while 1:
        data = conn.recv(8192)
        message.extend(data)
        if len(data) < 8192:
            break
        print("batch: ", data)
    conn.sendall(b'HTTP/1.1 200 OK\r\nDate: Mon, 27 Jul 2009 12:28:53 GMT\r\nServer: Apache/2.2.14 (Win32)\r\nLast-Modified: Wed, 22 Jul 2009 19:15:56 GMT\r\nContent-Length: 88\r\nContent-Type: text/html\r\nConnection: Closed\r\n\r\n<html><body>Got Here</body></html>')
    print(message)
    conn.close()
    results += [message]
    print("closing thread")
    s.close()

def displayPost(post):
    global title, label, sublabel, bf, fg
    if post is None:
        return

    if label is not None:
        #label.pack_forget()
        label.deleteLater()
        del label
        label = None

    if title is not None:
        title.deleteLater()
        del title
        title = None
    
    if sublabel is not None:
        sublabel.deleteLater()
        del sublabel
        sublabel = None

    name = post.fullname
    if name in history:
        nextFunc()
        return
    
    history.append(name)
    print('\n\n')
    print(post.url)
    print(post.title)
    title = QLabel(post.title)
    title.setWordWrap(True)
    sublabel = QLabel(post.subreddit_name_prefixed)
    layout.addWidget(sublabel)
    layout.addWidget(title)
    #url = "https://www.codeproject.com/KB/GDI-plus/ImageProcessing2/flip.jpg"
    url = post.url
    #url = "https://s3.amazonaws.com/mailbakery/wp-content/uploads/2015/06/26160411/sambag.gif"
    urlend = url[-4:]

    acceptedImages = [".png", ".jpg"]
    acceptedVideos = [".gif", "gifv"]
    #print("selftext:", post.selftext)
    #print("fullname:", post.fullname)
    print(vars(post))
    if post.selftext_html or post.is_self:
        label = QTextEdit(post.selftext_html)
        #label.setWordWrap(True)
        #label.setTextFormat(QtCore.Qt.RichText)
        label.setReadOnly(True)
        layout.addWidget(label)
        print("Showing self post")
    if url.endswith('.mp4') or url.startswith('https://v.redd.it/'):
        if post.media and 'reddit_video' in post.media and 'fallback_url' in post.media['reddit_video']:
            url = post.media['reddit_video']['fallback_url']
        
        label = QVideoWidget()
        r = requests.get(url)
        print("encoding: ", r.encoding)
        filename = '/tmp/file.mp4'
        print(filename)
        with open(filename, 'wb') as handle:
                for block in r.iter_content(1024):
                            handle.write(block)
                handle.close()
        mediaobj = QMediaPlayer(None, QMediaPlayer.VideoSurface)
        mediaobj.setMedia(QMediaContent(QUrl('/tmp/file.mp4')))
        mediaobj.setVideoOutput(label)
        layout.addWidget(label)
        label.show()
        mediaobj.play()
    elif urlend in acceptedImages:
        r = requests.get(url)
        filename = '/tmp/file.tmp'
        print(filename)
        with open(filename, 'wb') as handle:
                for block in r.iter_content(1024):
                            handle.write(block)
                handle.close()
        print("encoding: ", r.encoding)
        pm = QPixmap(filename)
        label = QLabel()
        label.setPixmap(pm.scaled(content[0], content[1], QtCore.Qt.KeepAspectRatio))
        layout.addWidget(label)
        print('Showing image post')
    
    elif urlend in acceptedVideos:
        if urlend == 'gifv':
            url=url[:-1]
        print("getting movie at: ", url)
        r = requests.get(url)
        print("encoding: ", r.encoding)
        filename = '/tmp/file.gif'
        print(filename)
        with open(filename, 'wb') as handle:
                for block in r.iter_content(1024):
                            handle.write(block)
                handle.close()
        qmv = QMovie(filename)
        label = QLabel();
        label.setMovie(qmv)
        layout.addWidget(label)
        qmv.start()
        print('Showing movie post')
    
    else:
        print("Using Browser")
        label = QWebView()
        label.load(QUrl(url))
        layout.addWidget(label)


def nextFunc():
    global poststream
    post = poststream.next()
    displayPost(post)





def backgroundColor(widget, color='white'):
    widget.configure(background=color)



def makeWindowScreenSize():
    w, h = master.winfo_screenwidth(), master.winfo_screenheight()
    master.geometry("%dx%d+0+0" % (w, h))
    master.minsize(w, h)



auth = openPickleJar('.Redidauth.pkl')    
#reddit = praw.Reddit(client_id=botCredentials.clientID, client_secret=botCredentials.clientSecret, user_agent='Redid 0.0.1/praw')
reddit = None
if auth:
    #print("REFRESHTOKEN: ", reddit.auth.implicit(auth['access_token'], int(auth['expires_in']), auth['scope']))
    print(auth)
    #refresh_tok = reddit.auth.authorize(auth['code'])
    reddit = praw.Reddit(client_id=app_client_ID, client_secret=app_secret, refresh_token=auth, user_agent='Redid 0.0.1/praw')
    print('REFRESHTOKEN : ', auth)
    poststream = reddit.front.hot()
    print("Logged in as:", reddit.user.me())
else:
    
    reddit = praw.Reddit(client_id=app_client_ID, client_secret=app_secret, redirect_uri='http://localhost:' + str(PORT), user_agent='Redid 0.0.1/praw')
    url = reddit.auth.url(['identity', 'edit', 'flair', 'history', 'modconfig', 'modflair', 'modlog', 'modposts', 'modwiki', 'mysubreddits', 'privatemessages', 'read', 'report', 'save', 'submit', 'subscribe', 'vote', 'wikiedit', 'wikiread'], 'abcd')#, duration='temporary')
    #url = 'https://www.reddit.com/api/v1/authorize?client_id=%s&response_type=code&state=%s&redirect_uri=%s&duration=permanant&scope=identity,edit,flair,history,modconfig,modflair,modlog,modposts,modwiki,mysubreddits,privatemessages,read,report,save,submit,subscribe,vote,wikiedit,wikiread' % (botCredentials.clientID, 'ABCD', 'http://localhost:56341')
    print(url)


    results = []



   
    from threading import Thread
    thread = Thread(target=getAuthorizationURL)

    label = QWebView()
    label.load(QUrl(url))

    import time
    #time.sleep(30)
    layout.addWidget(label)
    thread.start()
    timer = QTimer()
    frags = {}
    poststream = None
    timer.timeout.connect(printnew)
    timer.start(1000)


    #if label.page().mainFrame().toHtml() == '''<html><head></head><body><pre style="word-wrap: break-word; white-space: pre-wrap;">{"dest": "https://www.reddit.com/api/v1/authorize?client_id=s_mNL80tYVxdKw&amp;duration=temporary&amp;redirect_uri=http%3A%2F%2Flocalhost%3A38793&amp;response_type=token&amp;scope=identity&amp;state=abcd"}</pre></body></html>''':
    #    label.load(QUrl('''https://www.reddit.com/api/v1/authorize?client_id=s_mNL80tYVxdKw&amp;duration=temporary&amp;redirect_uri=http%3A%2F%2Flocalhost%3A38793&amp;response_type=token&amp;scope=identity&amp;state=abcd'''))




n.clicked.connect(nextFunc)
wid = QtWidgets.QWidget();
wid.setLayout(layout)
wid.show()


#nextFunc()




app.exec_()
